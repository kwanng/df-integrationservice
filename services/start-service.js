var nw = require('node-windows');
var path = require('path');

var Service = nw.Service;

// Create a new service object
var svc = new Service({
  name:'DF-Integrationservice',
  description: 'DF-Integrationservice',
  script: path.normalize('/shared/DF/df-integrationservice/src/server.js')
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  console.log('DF-Integrationservice Install complete');
  svc.start();
  console.log('DF-Integrationservice started');
});

// Listen for the "uninstall" event so we know when it's done.
svc.on('uninstall',function(){
  console.log('DF-Integrationservice uninstall complete.');
  console.log('DF-Integrationservice exists:', svc.exists);
});

// Uninstall or Install the service.
//svc.uninstall();
svc.install();