var Boom = Boom = require('boom'),
    logHandler = require('../handlers/log.handler');

var logRoutes = [
    {
        method: 'GET',
        path: '/log',
        handler: function (request, reply) {
            logHandler.get().then(function (result) {
                reply(result);
            }).catch(function (error) {
                reply(Boom.wrap(error, 400, error.message));
            })
        }
    },
    {
        method: 'DELETE',
        path: '/log',
        handler: function (request, reply) {
            logHandler.delete(request.payload).then(function(result){
                reply(result);
            }).catch(function(error){
                reply(Boom.wrap(error, 400, error.message));
            })
        }
    }
];

module.exports = logRoutes;
