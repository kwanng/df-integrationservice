var routes = [
    {
		path: '/{path*}',
		method: 'GET',
		handler: {
			directory: {
				path: './public',
				listing: false
			}
		}
	},
	{
        method: 'GET',
        path: '/',
        handler: function(request, reply) {
            return reply.view('index');
        }
    }
];

//let's make the routes available to the server
module.exports = routes;