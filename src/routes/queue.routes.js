var Boom = require('boom'),
    queueHandler = require('../handlers/queue.handler');

var queueRoutes = [
    {
        method: 'GET',
        path: '/queue/{id?}',
        handler: function (request, reply) {
            queueHandler.get(request.params.id).then(function(result){
                reply(result);
            }).catch(function(error){
                reply(Boom.wrap(error, 400, error.message));
            });
        }
    },
    {
        method: 'PUT',
        path: '/queue/{id}',
        handler: function (request, reply) {
            queueHandler.create(request.params.id).then(function(result){
                reply(result);
            }).catch(function(error){
                reply(Boom.wrap(error, 400, error.message));
            });
        }
    },
    {
        method: 'DELETE',
        path: '/queue/{id}',
        handler: function (request, reply) {
            queueHandler.delete(request.params.id).then(function(result){
                reply(result);
            }).catch(function(error){
                reply(Boom.wrap(error, 400, error.message));
            });
        }
    },
    {
        method: 'GET',
        path: '/pop',
        handler: function (request, reply) {
            queueHandler.pop().then(function(result){
                reply(result);
            }).catch(function(error){
                reply(Boom.wrap(error, 400, error.message));
            });
        }
    },
    {
        method: 'GET',
        path: '/pending',
        handler: function (request, reply) {
            queueHandler.getPending().then(function(result){
                reply(result);
            }).catch(function(error){
                reply(Boom.wrap(error, 400, error.message));
            });
        }
    },
    {
        method: 'GET',
        path: '/count',
        handler: function (request, reply) {
            queueHandler.getNumberPending().then(function(result){
                reply(result);
            }).catch(function(error){
                reply(Boom.wrap(error, 400, error.message));
            });
        }
    },
    {
        method: 'GET',
        path: '/remove',
        handler: function (request, reply) {
            queueHandler.removeAll().then(function(result){
                reply(result);
            }).catch(function(error){
                reply(Boom.wrap(error, 400, error.message));
            });
        }
    }
];

module.exports = queueRoutes;