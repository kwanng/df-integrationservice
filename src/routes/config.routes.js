var Boom = Boom = require('boom'),
    configHandler = require('../handlers/config.handler');

var configRoutes = [
    {
        method: 'GET',
        path: '/config',
        handler: function (request, reply) {
            configHandler.get().then(function (result) {
                reply(result);
            }).catch(function (error) {
                reply(Boom.wrap(error, 400, error.message));
            })
        }
    },
    {
        method: 'POST',
        path: '/config',
        handler: function (request, reply) {
            configHandler.update(request.payload).then(function(result){
                reply(result);
            }).catch(function(error){
                reply(Boom.wrap(error, 400, error.message));
            })
        }
    }
];

module.exports = configRoutes;
