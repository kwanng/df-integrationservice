require('dotenv').load();

var dfHandler = require('../handlers/df.handler');

var sessionId;
var assetId = '251356';
var data = {
    'custom.status': 'test',
    'custom.folder': 'myFolder',
    'custom.subfolder': 'mySubfolder'
};
var fileType = 'jpg';

describe('DF Handler', function() {
    it('should be able to authenticate with DF Studio.', function (done) {
        dfHandler.authenticate().then(function (result) {
            sessionId = result;
            expect(typeof result).toBe('string');
            expect(result.length).toBe(20);
            done();
        })
    });

    it('should set be able to update the metadata of a DF Studio asset.', function (done) {
        dfHandler.updateMetadata(sessionId, assetId, data).then(function (result) {
            expect(result['custom.status']).toBe(data['custom.status']);
            expect(result['custom.folder']).toBe(data['custom.folder']);
            expect(result['custom.subfolder']).toBe(data['custom.subfolder']);

            done();
        });
    });

    it('should get an array of asset ids based on a key value pair.', function (done) {
        dfHandler.getAssets(sessionId, 'custom.status', 'test').then(function (result) {
            expect(result instanceof Array).toBe(true);
            expect(result.length > 0).toBe(true); // we just updated one to have that status
            expect(result.indexOf(assetId) !== -1).toBe(true);
            done();
        });
    });

    it('should get the template from an asset id.', function (done) {
        dfHandler.getTemplate(sessionId, assetId).then(function (result) {
            expect(result.template).toBe(data['custom.folder']);
            expect(result.subtemplate).toBe(data['custom.subfolder']);
            done();
        });
    });

    it('should get the files from an asset id.', function (done) {
        dfHandler.getFiles(sessionId, assetId).then(function (result) {
            expect(result instanceof Array).toBe(true);
            expect(result.length > 0).toBe(true);
            expect(typeof result[0].fileType).toBe('string');
            done();
        });
    });

    it('should get the all of the metadata from an asset id.', function (done) {
        dfHandler.getMetadata(sessionId, assetId).then(function (result) {
            // there are way to many fields to check. Just choose one we know the value of.
            expect(result['custom.status']).toBe(data['custom.status']);
            done();
        });
    });

    it('should get the file name from an asset id.', function (done) {
        dfHandler.getFileName(sessionId, assetId).then(function (result) {
            expect(typeof result).toBe('string');
            done();
        });
    });

    it('should get the file URL from an asset id and a file type.', function (done) {
        dfHandler.getFileUrl(sessionId, assetId, fileType).then(function (result) {
            expect(typeof result).toBe('string');
            done();
        });
    });
});