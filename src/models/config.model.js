'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ConfigSchema = new Schema({
    MaxNumberOfWorkers:  { type: Number },
    ScannerFrequency: { type: Number },
    TasksFrequency: { type: Number },
    StagingFrequency: { type: Number },        
    IngestTimeOut: { type: Number },
    TemplateTimeOut: { type: Number },
    UpdateWaitTimeOut: { type: Number },
    IngestInterval: { type: Number },
    TemplateInterval: { type: Number }

},{
    collection:'Config'
});

mongoose.model('Config', ConfigSchema);
