'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PerformanceSchema = new Schema({
    MaxNumberOfWorkers:  { type: Number },
    ScannerFrequency: { type: Number },
    QueueWorkerFrequency: { type: Number },    
    IngestInterval: { type: Number },
    TemplateInterval: { type: Number },
    StartTime: { type: String },
    StopTime: { type: String },
    Processed: { type: Number }
},{
    collection:'Performance'
});

mongoose.model('Performance', PerformanceSchema);
