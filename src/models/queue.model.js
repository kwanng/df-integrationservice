'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var QueueSchema = new Schema({
    Id:  { type: String, index: { unique: true }},
    isPending: { type: Boolean, default: true},
    Source: { type: String },
    CreatedOn: {
        type: Date, 
  		default: Date.now,
        index: true  		
    }
},{
    collection:'Queue'
});

mongoose.model('Queue', QueueSchema);
