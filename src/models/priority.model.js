'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PrioritySchema = new Schema({
    type:  { type: String, index: { unique: true }},
    order: { type: Number}
},{
    collection:'Priority'
});

mongoose.model('Priority', PrioritySchema);
