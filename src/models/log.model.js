'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LogSchema = new Schema({
    message: String,
    timestamp: Date,
    level: String,
    meta: Schema.Types.Mixed
},{
    collection:'Log'
});

mongoose.model('Log', LogSchema);
