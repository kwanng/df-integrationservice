(function() {
    'use strict';

    const async = require('async');
    const queueHandler = require('../handlers/queue.handler');
    const dfHandler = require('../handlers/df.handler');

    const request = require('request');
    const fs = require('fs');
    const path = require('path');

    const live = 'live';
    const rejected = 'rejected';

    const IngestTimeOut = 10000;
    const TemplateTimeOut = 10000;
    const UpdateWaitTimeOut = 1;

    var stopTimers = function(intervalID, timeOutID) {
        clearInterval(intervalID);
        clearTimeout(timeOutID);
    };

    var pollIngest = function(fileName, filePath, cb) {

        var ingestFilePath = path.join(filePath, '.Ingest Folder', fileName);

        var ingestJob = new job();

        var intervalTask = function() {
            try {
                fs.accessSync(ingestFilePath, fs.R_OK | fs.W_OK);
            }
            catch (err) {
                if (err.code !== 'ENOENT') { logger.info(err); }

                ingestJob.stop();
                cb(live);
            }
        };
        var timeoutTask = function() {
            logger.info('ingest timeout', ingestFilePath);
            ingestJob.stop();
            cb(rejected);
        };

        ingestJob.start(
            process.config.IngestInterval * 1000,
            intervalTask,
            IngestTimeOut,
            timeoutTask
        );
    };

    //scan template folder for file changes
    var pollTemplate = function(fileName, filePath, cb) {

        var templateFilePath = path.join(filePath, fileName);
        var templateJob = new job();

        var intervalTask = function() {
            try {
                fs.accessSync(templateFilePath, fs.R_OK | fs.W_OK);
            }
            catch (err) {
                if (err.code !== 'ENOENT') { logger.info(err); }

                templateJob.stop();
                cb(live);
            }
        };

        templateJob.start(
            process.config.TemplateInterval * 1000,
            intervalTask
        );
    }

    exports.execute = function(assetId, fileName, templatePath, callback) {
        async.waterfall([
            //check template folder
            function(next) {
                console.log('poll template', assetId);

                pollTemplate(fileName, templatePath, function(status) {
                    next(null, status);
                });
            },
            //check ingest folder
            function(status, next) {

                console.log('poll ingest', assetId);

                pollIngest(fileName, filePath, function(status) {
                    next(null, status);
                });
                
            },
            //Authenticate
            function(status, next) {
                dfHandler.authenticate().then(function(sessionId) {
                    next(null, status, sessionId);
                }).catch(function(err) {
                    next(err);
                });
            },
            //update df status
            function(status, sessionId, next) {
                dfHandler.updateMetadata(sessionId, assetId, { 'custom.status': status }).then(function(result) {
                    next();
                }).catch(function(err) {
                    next(err);
                });
            },
            //delete id from queue
            function(next) {
                //wait for df to get updated before dequeueing

                setTimeout(function() {
                    queueHandler.delete(assetId).then(function() {
                        next(null, assetId);
                    }).catch(function(err) {
                        next(err);
                    });

                }, (UpdateWaitTimeOut * 1000));
            },
        ], callback);

    };
})();

