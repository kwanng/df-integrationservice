(function () {
    'use strict';

    var async = require('async');
    var dfHandler = require('../handlers/df.handler');
    const threshold = 9;

    exports.execute = function (callback) {

        var sessionId;
        var assetIds;

        async.waterfall([
            //Authenticate
            function (next) {
                dfHandler.authenticate().then(function (result) {
                    sessionId = result;
                    //console.log(sessionId);
                    next();
                }).catch(function (err) {
                    console.log(err.message);
                });
            },
            // get assets with status of 'live'
            function (next) {
                dfHandler.getAssets(sessionId).then(function (result) {
                    assetIds = result;
                    next();
                }).catch(function (err) {
                    console.log(err.message);
                });
            },         
            // set assets with status of 'publish'
            function (next) {

                if (assetIds.length > threshold) {
                    assetIds = assetIds.slice(0, threshold);
                }

                async.each(assetIds, function (assetId, eachNext) {

                    dfHandler.updateMetadata(sessionId, assetId, { 'custom.status': 'publish' }).then(function (result) {

                        console.log(assetId + ' updated');

                        eachNext();

                    }).catch(function (err) {
                        console.log(err.message);
                        eachNext(err);
                    });

                }, function (err) {

                    if (err) {

                        next(err);

                    } else {
                        next();
                    }
                });

            },
            function () {

                console.log('assets set to publish');

                if (callback) {
                    callback(threshold);
                }

            }
        ]);
    };
})();