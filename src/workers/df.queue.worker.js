(function() {
    'use strict';

    var async = require('async');
    var request = require('request');
    var fs = require('fs');
    var path = require('path');

    var queueHandler = require('../handlers/queue.handler');
    var dfHandler = require('../handlers/df.handler');
    var conf = require('../config/app.config.js');

    exports.execute = function (assetId, priorities, callback) {
        
        console.log('DF begin', assetId);

        async.waterfall([
            function(next) {
                next(null,assetId);
            },
            //Authenticate
            function(assetId, next) {
                dfHandler.authenticate().then(function(sessionId) {                    
                    next(null, assetId, sessionId);
                }).catch(function(err) {
                    next(err);
                });
            },
            //Get file type
            function(assetId, sessionId, next) {

                dfHandler.getFiles(sessionId, assetId).then(function(result) {
                    var fileType = 'jpg';

                    if (result) {
                        fileType = result[0].fileType;
                    }

                    priorities.forEach(function(type) {
                        result.forEach(function(file) {
                            if (file.fileType == type) {
                                fileType = type;
                            }
                        })
                    }, this);

                    next(null, assetId, sessionId, fileType);

                }).catch(function(err) {
                    next(err);
                });
            },
            //Get file name
            function(assetId, sessionId, fileType, next) {
                dfHandler.getFileName(sessionId, assetId).then(function(fileName) {
                    fileName += '.' + fileType;
                    next(null, assetId, sessionId, fileType, fileName);
                }).catch(function(err) {
                    next(err);
                });
            },
            //Get template
            function(assetId, sessionId, fileType, fileName, next) {
                dfHandler.getTemplate(sessionId, assetId).then(function(result) {
                    
                    var templatePath = conf.asset_destination;                    
                    
                    var template = 'im_hotfolder'; //default
                    
                    var subTemplate = 'dfi';
                    /*
                                            if (result) {
                                                if (result.template)
                                                    template = result.template;
                    
                                                if (result.subtemplate)
                                                    subTemplate = result.subtemplate;
                                            }
                    */
                    templatePath += template + '\\' + subTemplate;

                    next(null, assetId, sessionId, fileType, fileName, templatePath);

                }).catch(function(err) {
                    next(err);
                });
            },
            //Get file url
            function(assetId, sessionId, fileType, fileName, templatePath, next) {
                dfHandler.getFileUrl(sessionId, assetId, fileType).then(function(fileUrl) {
                    next(null, assetId, fileName, templatePath, fileUrl);
                }).catch(function(err) {
                    next(err);
                });
            },
            //Get file
            function(assetId, fileName, templatePath, fileUrl, next) {
                if (!fileUrl) {
                    next({ message: 'No file url' });
                }
                else {

                    var options = {
                        url: fileUrl,
                        method: 'GET'
                    };

                    var filePath = templatePath + '\\' + fileName;
                    filePath = path.normalize(filePath);

                    var streamAsset = function() {

                        var stream = request(options,
                            function(error, response, body) {
                                if (error) {
                                    next(error);
                                }
                            }).pipe(fs.createWriteStream(filePath));

                        stream.on('finish', function() {
                            next(null, assetId, fileName, templatePath);
                        });

                        stream.on('error', function(error) {
                            next(error);
                        });

                    };

                    fs.exists(templatePath, function(exists) {
                        if (!exists) {
                            fs.mkdir(templatePath, streamAsset);
                        } else {
                            streamAsset();
                        }
                    });
                }

            }//call ingest worker with asset info to monitor the hot folders
        ],  function(error, assetId, fileName, templatePath){
        
            callback(error, assetId, fileName, templatePath);   
        
        } );
        
    }

})();
