(function () {
    'use strict';

    var async = require('async');
    var queueHandler = require('../handlers/queue.handler');
    var dfHandler = require('../handlers/df.handler');

    exports.execute = function (callback) {

        async.waterfall([
            //Authenticate
            function (next) {
                dfHandler.authenticate().then(function (sessionId) {
                    next(null, sessionId);
                }).catch(function (err) {
                    next(err);
                });
            },
            // get assets with status of 'publish'
            function (sessionId, next) {
                dfHandler.getAssets(sessionId, 'custom.status', 'publish').then(function (assetIds) {
                    next(null, assetIds);
                }).catch(function (err) {
                    next(err);
                });
            },
            // try to insert them in the queue
            function (assetIds, next) {               
                             
                if(assetIds.length > 0){
                    queueHandler.bulkInsert(assetIds, 'DF').then(function (result) {
                        next(null, result)
                    }).catch(function(err){
                        next(err);
                    });    
                }
                else{
                    next();
                }                
            }
        ], callback);
    };
})();
