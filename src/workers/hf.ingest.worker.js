(function () {
    'use strict';

    var async = require('async');
    var fs = require('fs');
    var path = require('path');
    var logger = require('log4js').getLogger('app');
    var mkdirp = require('mkdirp');

    var queueHandler = require('../handlers/queue.handler');
    var job = require('../handlers/job.handler');
    
    //var IngestTimeOut = 1000 * 60 * 10; //ms s m
    var TemplateTimeOut = 600;
    var UpdateWaitTimeOut = 1;

    //scan .ingest folder for file changes
    var pollIngest = function (fileName, filePath, sourcePath, ingestTimeout, cb) {

        //logger.info('Timeout Value:', ingestTimeout);
        
        var ingestFilePath = path.join(filePath, '.Ingest Folder', fileName);
        
        var ingestJob = new job();
        
        var intervalTask = function () {
            try {
                fs.accessSync(ingestFilePath, fs.R_OK | fs.W_OK);
            }
            catch (err) {
                if (err.code !== 'ENOENT') { logger.info(err); }
                
                ingestJob.stop();
                cb();
            }
        };
        var timeoutTask = function () {
            logger.info('Timed out waiting on ingest', ingestFilePath);
            ingestJob.stop();            
            
            var sourceFilePath =  path.dirname(sourcePath);
            
            sourceFilePath = path.join(sourceFilePath, '_rejected_');
        
            mkdirp(sourceFilePath, function(err) {
                
                    var destinationFilePath = path.join(sourceFilePath,fileName);
                    logger.info('Moving to '+destinationFilePath);
                    
                    fs.rename(ingestFilePath, destinationFilePath, function (err) {
                        cb();                           
                    });
            });
        };

        ingestJob.start(
            process.config.IngestInterval * 1000, 
            intervalTask,
            ingestTimeout, 
            timeoutTask
        );
    };

    //scan template folder for file changes
    var pollTemplate = function (fileName, filePath, cb) {

        var templateFilePath = path.join(filePath, fileName);
        var templateJob = new job();
        
        var intervalTask =  function () {
            try {
                fs.accessSync(templateFilePath, fs.R_OK | fs.W_OK);
            }
            catch (err) {
                if (err.code !== 'ENOENT') { logger.info(err); }
                
                templateJob.stop();
                cb();
            }
        };

        templateJob.start(
            process.config.TemplateInterval * 1000, 
            intervalTask
        );
    }

    //hot folder ingest worker
    exports.execute = function (id, ingestTimeout, callback) {        
        
        logger.info(id);

        var sourcePath = id;
        
        var destPath = sourcePath.replace('_staging_\\', '');
        
        var filePath =  path.dirname(destPath);
        
        var fileName = path.basename(destPath);
        
        async.waterfall([
            
            //move file to hotfolder
            function (next) {
                
                logger.info('renaming file', destPath);
                
                fs.rename(sourcePath, destPath, function (err) {
                    
                    if (err) {
                        next(err);
                    }
                    else {
                        next();
                    }
                });
            },
            
            //check template folder
            function (next) {
                logger.info('poll template', fileName);
                pollTemplate(fileName, filePath, function () {
                    next();
                });
            },
                       
            //check ingest folder
            function (next) {
                logger.info('poll ingest', fileName);
                pollIngest(fileName, filePath, sourcePath, ingestTimeout, function () {
                    next();
                });
            },
        ], function (err, result) {            
            //delete id from queue
            queueHandler.delete(id).then(function () {
                callback();
            }).catch(function (err) {
                callback(err);
            });
        });
    };
})();