//this will initialize the angular app;
(function () {

    'use strict';
    
    angular.module('app', ['ngMaterial', 'ui.router'])

    .run(function(_) {
        // invoke the lodash library
    })
    .factory('_', function() {
        return window._;
    }).run(function(moment){
        // invoke the moment library
    })
    .factory('moment', function(){
        return window.moment;
    });

})();