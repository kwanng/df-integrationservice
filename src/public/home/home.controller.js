(function () {
    'use strict';
    
    var app = angular.module('app');
    app.controller('HomeController',['$scope', '$http', function ($scope, $http) {
        $http.get('/config').then(function(result){
            $scope.config = result.data;
            delete $scope.config._id;
            delete $scope.config.__v;
        })

        $scope.save = function(){
            $http.post('/config', $scope.config).then(function(result){
                $scope.config = result.data;
                delete $scope.config._id;
                delete $scope.config.__v;
            })
        }
        
    }]);
}());
