var hapi = require('hapi');

// force the working directory to be this one
process.chdir(__dirname);

// Create hapi server instance
var server = new hapi.Server();

// add connection parameters
server.connection({
    port: 1300
});

//remove max listerners, related to eventemitter 
require('events').EventEmitter.prototype._maxListeners = 0;

// load the .env file
require('dotenv').load();

// configure and load the logger
var log4js = require('log4js');
log4js.configure('./config/log4js.config.json');
var logger = log4js.getLogger('app');
//var logger = require('./logger');
//logger.add();

// load mongoose
var mongoose = require('mongoose');

// load the database models
var db = {
    Queue: require('./models/queue.model.js'),
    Config: require('./models/config.model.js'),
    Priority: require('./models/priority.model.js'),
    Performance: require('./models/performance.model.js'),
    Log: require('./models/log.model.js')
};


var conf = require('./config/app.config.js');

// connect to the database
mongoose.connect(conf.db_url, { user: process.env.DB_USER, pass: process.env.DB_PASS });
    db.database = mongoose.connection;
    db.database.on('disconnected', function () { console.log('DB Disconnected!') });
    db.database.on('error', function () { console.log('DB Connection Error!') });
    db.database.on('connected', function () {

    //logger.info('DB Connected');

    // start all the processing
    require('./processor.js').start();
});

server.register(require('vision'), function (err) {
    if (err) {
        logger.error('Server Views error', err);
    }

	server.views({
		engines:{
			html: require('handlebars')
		},
		path: './views',  // my views live here
        layoutPath: 'views/layout',  // my layouts live here.  for this kit, I will only use one
        layout: 'default',  // my default layout is 'default'
        partialsPath: 'views/partials'    // my partial views live in this directory
	})
});


// start the server and register the routes.
// Note: This will likely be removed before going to production
server.register(require('inert'), function (error) {
    if (error) {
        logger.error('Server Route Error', error);
        //throw error;
    }

    server.route(require('./routes/routes.js'));
    server.route(require('./routes/queue.routes'));
    server.route(require('./routes/df.routes'));
    server.route(require('./routes/config.routes'));
    server.route(require('./routes/log.routes'));

    server.start(function () {
        var env = process.env.NODE_ENV;
        var url = server.info.uri;

        logger.info('DF Integration Service [%s %s]', env, url);
    });
});