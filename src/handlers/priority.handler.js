(function(){

    'use strict'
    var mongoose = require('mongoose'),
        Priority = mongoose.model('Priority');

    var priorityHandler = {};

    priorityHandler.get = function(){
        
        return new Promise(function(resolve, reject) {
            Priority.find({},'type').sort('order').exec(function (err, results) {
                if (err)
                    reject(err);
                else {
                    var priorities = [];
                    results.forEach(function (item) {
                        priorities.push(item.type);
                    }, this);

                    resolve(priorities);
                }
            });
        });
    };


    module.exports = priorityHandler;

})();