(function(){

    'use strict'
    var mongoose = require('mongoose'),
        Config = mongoose.model('Config');

    var configHandler = {};

    configHandler.get =  function() {
        return new Promise(function(resolve, reject) {
            Config.findOne().exec(function (err, result) {
                if (err)
                    reject(err);
                else {
                    resolve(result);
                }
            });
        });
    };

    configHandler.update =  function(newConfig) {
        return new Promise(function(resolve, reject) {
            configHandler.get().then(function(config){
                for(var i in newConfig){
                    if (config[i])
                        config[i] = newConfig[i];
                }
                config.save(function (err, product, numberAffected) {
                    if (err)
                        reject(err);
                    else if (numberAffected > 0)
                        resolve(product)
                    else
                        reject({message: 'No entry was created'});
                });
            });
        });
    };

    configHandler.makeConfigGlobal = function (destination){
        return new Promise(function(resolve, reject) {
            Config.findOne(function (err, result) {
                if (!err && result) {
                    for (var i in result._doc) {
                        if (i[0] != '_')
                            destination[i] = result._doc[i];
                    }
                    resolve('Success!');
                }
            });
        });
    };

    module.exports = configHandler;

})();
