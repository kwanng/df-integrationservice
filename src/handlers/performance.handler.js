(function() {

    'use strict'
    var mongoose = require('mongoose'),
        PerformanceModel = mongoose.model('Performance');

    var performanceHandler = {};

    performanceHandler.create = function(performanceData) {

        var performance = new PerformanceModel();

        performance.MaxNumberOfWorkers = performanceData.MaxNumberOfWorkers;
        performance.ScannerFrequency = performanceData.ScannerFrequency;
        performance.QueueWorkerFrequency = performanceData.QueueWorkerFrequency;
        performance.IngestInterval = performanceData.IngestInterval;
        performance.TemplateInterval = performanceData.TemplateInterval;
        performance.StartTime = performanceData.StartTime;
        performance.StopTime = performanceData.StopTime;
        performance.Processed = performanceData.Processed;

        return new Promise(function(resolve, reject) {
            performance.save(function(err, result) {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    };

    module.exports = performanceHandler;

})();
