(function() {

    'use strict'

    var requestJs = require('request');
    var dfHandler = {};

    dfHandler.authenticate = function() {
        var options = {
            url: 'http://demo.dfstudio.com/api/v1/session',
            method: 'POST',
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': 'brandcentral'
            },
            body: {
                'username': process.env.DF_USERNAME,
                'account': process.env.DF_ACCOUNT,
                'password': process.env.DF_PASSWORD
            }
        };
        return new Promise(function(resolve, reject) {
            requestJs(options, function(error, response, body) {
                if (error)
                    reject(error);
                else if (body) {
                    var slash = body.lastIndexOf('/');
                    var dot = body.lastIndexOf('.')
                    resolve(body.substring(++slash, dot));
                } else
                    reject({ message: 'Could not find a session id.' });
            });
        });
    };

    var getAssetIds = function(data) {
        var assetIds = [];
        for (var i in data) {
            assetIds.push(data[i].id);
        }
        return assetIds;
    }

    dfHandler.getAssets = function(sessionId, key, value) {
        var options = {
            url: 'http://demo.dfstudio.com/api/v1/session/' + sessionId + '/assets.js?' + key + '=' + value,
            method: 'GET',
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': 'brandcentral'
            }
        };
        return new Promise(function(resolve, reject) {
            requestJs(options, function(error, response, body) {
                if (error)
                    reject(error);
                else if (body)
                    resolve(getAssetIds(body.returned));
                else
                    reject(response);
            });
        });
    };

    dfHandler.getMetadata = function(sessionId, assetId) {
        var options = {
            url: 'http://demo.dfstudio.com/api/v1/session/' + sessionId + '/assets/' + assetId + '/metadata.js',
            method: 'GET',
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': 'brandcentral'
            }
        };
        return new Promise(function(resolve, reject) {
            requestJs(options, function(error, response, body) {
                if (error)
                    reject(error);
                else if (body)
                    resolve(body);
                else
                    reject(response);
            });
        });
    };

    dfHandler.getTemplate = function(sessionId, assetId) {
        return new Promise(function(resolve, reject) {
            dfHandler.getMetadata(sessionId, assetId).then(function(metadata) {
                var result = {};
                result.template = metadata['custom.folder'];
                result.subtemplate = metadata['custom.subfolder'];
                resolve(result);
            }).catch(function(err) {
                reject(err);
            })
        });
    };

    dfHandler.getStatus = function(sessionId, assetId) {
        return new Promise(function(resolve, reject) {
            dfHandler.getMetadata(sessionId, assetId).then(function(metadata) {
                
                var status = metadata['custom.status'];                
                resolve(result);
            }).catch(function(err) {
                reject(err);
            })
        });
    };

    dfHandler.getFiles = function(sessionId, assetId) {
        var options = {
            url: 'http://demo.dfstudio.com/api/v1/session/' + sessionId + '/assets/' + assetId + '/files',
            method: 'GET',
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': 'brandcentral'
            }
        };
        return new Promise(function(resolve, reject) {
            requestJs(options, function(error, response, body) {
                if (error)
                    reject(error);
                else if (body)
                    resolve(body);
                else
                    reject(response);
            });
        });
    };

    dfHandler.getAsset = function(sessionId, assetId) {
        var options = {
            url: 'http://demo.dfstudio.com/api/v1/session/' + sessionId + '/asset/' + assetId,
            method: 'GET',
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': 'brandcentral'
            }
        };
        return new Promise(function(resolve, reject) {
            requestJs(options, function(error, response, body) {
                if (error)
                    reject(error);
                else if (body)
                    resolve(body);
                else
                    reject({ message: 'The filename could not be found.' })
            });
        });
    };

    dfHandler.getFileName = function(sessionId, assetId) {
        return new Promise(function(resolve, reject) {
            dfHandler.getAsset(sessionId, assetId).then(function(result) {
                resolve(result.name);
            }).catch(function(err) {
                reject(err);
            })
        });
    };

    dfHandler.getFileUrl = function(sessionId, assetId, fileType) {
        var options = {
            url: 'http://demo.dfstudio.com/api/v1/session/' + sessionId + '/asset/' + assetId + '/file/' + fileType,
            method: 'GET',
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': 'brandcentral'
            }
        };
        return new Promise(function(resolve, reject) {
            requestJs(options, function(error, response, body) {
                if (error)
                    reject(error);
                else if (body)
                    resolve(body.fileUrl);
                else
                    reject({ message: 'The filename could not be found.' })
            });
        });
    };

    dfHandler.updateMetadata = function(sessionId, assetId, data) {
        var options = {
            url: 'http://demo.dfstudio.com/api/v1/session/' + sessionId + '/assets/' + assetId + '/metadata.js',
            method: 'POST',
            json: true,
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': 'brandcentral'
            },
            body: data
        };
        return new Promise(function(resolve, reject) {
            requestJs(options, function(error, response, body) {
                if (error)
                    reject(error);
                else if (body)
                    resolve(body);
                else
                    reject({ message: 'Something went wrong.' });
            });
        });
    };

    module.exports = dfHandler;

})();