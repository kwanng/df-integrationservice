(function() {
    'use strict';

    module.exports = function Job() {
        
        var self = this;
        
        var intervalID = null;
        
        var timeoutID = null;
        
        self.startNow = function(intervalTime, intervalTask, timeoutTime, timeoutTask) {
            
            intervalTask();
            
            self.start(intervalTime, intervalTask, timeoutTime, timeoutTask);
        };
        
        self.start = function(intervalTime, intervalTask, timeoutTime, timeoutTask) {
        
            intervalID = setInterval(intervalTask, intervalTime);

            if (timeoutTime) {
                timeoutID = setTimeout(function() {
                    self.stop();

                    if (timeoutTask) {
                        timeoutTask();
                    };

                }, timeoutTime);
            }
        };
        
        self.stop = function() {

            if (intervalID) {
                clearInterval(intervalID);
            }

            if (timeoutID) {
                clearTimeout(timeoutID);
            }
        };
    }
})();