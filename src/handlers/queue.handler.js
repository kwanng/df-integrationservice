(function() {

    'use strict'
    var mongoose = require('mongoose');
    var Queue = mongoose.model('Queue');
    var logger = require('log4js').getLogger('app');

    var queueHandler = {};

    queueHandler.get = function(id) {
        var query = {};

        if (id) {
            query.Id = id;
        }

        return new Promise(function(resolve, reject) {
            Queue.findOne(query).sort('_id').exec(function(err, result) {
                if (err)
                    reject(err);
                else {
                    resolve(result.Id);
                }
            });
        });
    };

    queueHandler.getPending = function() {
        return new Promise(function(resolve, reject) {

            Queue.findOneAndUpdate({ isPending: true }, { isPending: false }).sort('_id').exec(function(err, result) {
                if (err)
                    reject(err);
                else {
                    
                    //logger.info('db got:', result.Id);
                    var id, source; 
                    if(result){
                        id = result.Id;
                        source = result.Source;
                    }
                    
                    resolve({ Id: id, Source: source });
                }
              
            });
        });
    }

    queueHandler.create = function(id) {
        var queueItem = new Queue({ Id: id });
        return new Promise(function(resolve, reject) {
            queueItem.save(function(err, product, numberAffected) {
                if (err)
                    reject(err);
                else if (numberAffected > 0)
                    resolve(product)
                else
                    reject({ message: 'No queue entry was created' });
            });
        });
    };

    queueHandler.delete = function(id) {
        return new Promise(function(resolve, reject) {
            Queue.findOneAndRemove({ Id: id }).exec(function(err, result) {
                
                //logger.info('db delete:', id, err, result);
                
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    };

    queueHandler.pop = function() {
        return new Promise(function(resolve, reject) {
            Queue.findOneAndRemove().sort('_id').exec(function(err, result) {
                if (err)
                    reject(err);
                else
                    resolve(result.Id);
            });
        });
    };

    queueHandler.bulkInsert = function(ids, source) {
        var items = [];
        for (var i in ids) {
            items.push({ Id: ids[i], isPending: true, Source: source });
        }
        return new Promise(function(resolve, reject) {
            
            Queue.collection.insert(items, { ordered: false }, function(err, docs) {
                
                    if (docs && docs.insertedCount > 0) {
                        resolve({ message: docs.insertedCount + ' new item(s) added to queue', inserted: docs.insertedCount });
                    }
                    else {
                        resolve({ message: 'No queue entries added.' });
                    }                
            });
        });
    };

    queueHandler.getNumberPending = function() {
        return new Promise(function(resolve, reject) {
            Queue.count({ isPending: true }).exec(function(err, result) {
                if (err)
                    reject(err);
                else
                    resolve(result);

            });
        })
    };

    queueHandler.removeAll = function() {
        return new Promise(function(resolve, reject) {
            Queue.remove().exec(function(err, result) {
                if (err)
                    reject(err);
                else
                    resolve(result);

            });
        })
    };


    module.exports = queueHandler;

})();