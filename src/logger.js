'use strict';

// Require Statements
var winston = require('winston');
require('winston-mongodb').MongoDB;

exports.add = function () {
    // log to file
    winston.add(winston.transports.File, {filename: 'api.log', handleExceptions: true});

    // log to MongoDB
    winston.add(winston.transports.MongoDB, {
        db: process.env.DB_URL,
        collection: 'Log',
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        handleExceptions: true});
};


/**
 * Helper function that generates the request's url
 */
winston.requestPath = function(req) {
    return req.protocol + '://' + req.hostname + req.originalUrl;
};

//winston.exitOnError = onEaddrinuse;
winston.exitOnError = function(err) { return err.code === 'EADDRINUSE'; };

var log = function(level, message, assetId){
    var meta = {};
    if (assetId)
        meta.assetId = assetId;
    winston.log(level, message, meta);
}

exports.info = function(message, assetId){
    log('info', message, assetId);
}

exports.error = function(error, assetId){
    log('error', error, assetId);
};
