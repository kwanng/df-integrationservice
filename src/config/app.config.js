'use strict';

var conf = {

	development: {
		asset_source: '//twdhnas/telescope_nonprod/dev2/_staging_/'
        ,asset_destination: '//twdhnas.turner.com/telescope_nonprod/dev2/'
        ,db_url: 'mongodb://ds059804.mlab.com:59804/bc-dfstudio'
	},
	qa: {
        asset_source: '//twdhnas/telescope_nonprod/QA2/_staging_/'
        ,asset_destination: '//twdhnas.turner.com/telescope_nonprod/QA2/'
        ,db_url: 'mongodb://ds013981.mlab.com:13981/bc-dfstudio-qa'		
	},
    production: {
        asset_source: '//twdhnas/telescope_prod/_staging_/'
        ,asset_destination: '//twdhnas.turner.com/telescope_prod/'
        ,db_url: 'mongodb://ds021701.mlab.com:21701/bc-dfstudio-prod'		
	}
};

var env = process.env.NODE_ENV;

module.exports = conf[env] || conf.development;