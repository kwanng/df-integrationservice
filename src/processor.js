(function() {
    'use strict';

    var async = require('async');
    var path = require('path');
    var fs = require('fs');
    var moment = require('moment');
    var nodedir = require('node-dir');
    var logger = require('log4js').getLogger('app');
    var _ = require('lodash');
    var mkdirp = require('mkdirp');

    var queueHandler = require('./handlers/queue.handler.js');
    var job = require('./handlers/job.handler.js');
    var dfScanner = require('./workers/df.scanner.js');

    var dfIngestWorker = require('./workers/df.ingest.worker.js');
    var dfWorker = require('./workers/df.queue.worker.js');
    var hfIngestWorker = require('./workers/hf.ingest.worker.js');

    var conf = require('./config/app.config.js');

    var init_priorities;

    const batchSize = 50;
    const minIngestTimeout = 1000 * 60 * 3; 

    var init = function() {
        return new Promise(function(resolve, reject) {
            async.waterfall([
                // make the config global
                function(next) {
                    require('./handlers/config.handler').makeConfigGlobal(process.config).then(function(result) {
                        logger.info('Configuration globalized!');
                        next();
                    }).catch(function(err) {
                        next(err);
                    });
                },
                //empty the queue
                function(next) {
                    queueHandler.removeAll().then(function() {
                        logger.info('Queue emptied!');
                        next();
                    }).catch(function(err) {
                        next(err);
                    });
                },
                // load the priorities
                function(next) {
                    require('./handlers/priority.handler').get().then(function(result) {
                        init_priorities = result;
                        next();
                    }).catch(function(err) {
                        next(err);
                    });
                }

                // run test
                // , function(next) {
                //     var testWorker = require('./workers/testWorker');
                //     testWorker.execute(function(threshold) {
                //         next();
                //     });
                // }

            ], function(err) {
                if (err)
                    reject(err)
                else
                    resolve();
            });
        });
    };

    var execute = function() {

        var scannerIntervalTime = process.config.ScannerFrequency * 1000;
        var stagingIntervalTime = process.config.StagingFrequency * 1000;
        var tasksIntervalTime = 30 * 1000;

        // var scannerJob = new job();

        // scannerJob.start(scannerIntervalTime
        //     , function() {

        //         dfScanner.execute(function(err, result) {
        //             if (err) {
        //                 logger.error(err);
        //             } else {

        //                 console.log('Scanner completed');

        //                 if (result) {
        //                     console.log(result);
        //                 }

        //             }
        //         });
        //     });

        var stagingJob = new job();

        var scanStaging = function() {

//            logger.info('scan staging');

            //stagingJob.stop();

            function getDirectories(srcpath) {
                return fs.readdirSync(srcpath).filter(function(file) {
                    return fs.statSync(path.join(srcpath, file)).isDirectory();
                });
            };

            var source = path.normalize(conf.asset_source);

            var destination = path.normalize(conf.asset_destination);

            var source_dirs = getDirectories(source);

            var destination_dirs = getDirectories(destination);

            //only work on staging folders that are defined as hot folders in destination
            var correlated_dirs = _.intersection(source_dirs, destination_dirs);

            async.waterfall([
                //create new subdirs
                function(next) {

                    async.each(correlated_dirs, function(dirname, callback) {
                        var correlated_path = path.join(source, dirname);

                        nodedir.subdirs(correlated_path, function(err, subdirs) {

                            if (err) {
                                if (err.code !== 'ENOENT') {
                                    logger.info(err);
                                }

                                next(err);
                            }
                            else if (subdirs && subdirs.length > 0) {

                                async.each(subdirs, function(subdir, cb) {
                                    
                                    if (subdir.indexOf("_rejected_") < 0) {
                                        
                                        var newPath = subdir.replace('_staging_\\', '');
                                    
                                        mkdirp(newPath, function(err) {
                                            if (err) {
                                                logger.error('mkdirp', err);
                                                cb(err);
                                            } else {
                                                cb();
                                            }
                                        });
                                    }else{
                                        //dont make folders for rejected
                                        cb();
                                    }

                                },
                                    function(err) {
                                        callback(err);
                                    });
                            }
                            else {
                                callback();
                            }

                        });//subdirs
                    }, function(err) {
                        next(err);
                    });
                },

                //get files and bulk insert into queue
                function(next) {

                    //console.log(correlated_dirs);

                    async.each(correlated_dirs, function(dirname, callback) {

                        var correlated_path = path.join(source, dirname);

                        nodedir.files(correlated_path, function(err, files) {

                            if (err) {
                                if (err.code !== 'ENOENT') {
                                    logger.error('dir files:', err, correlated_path);
                                }

                                next(err);
                            }
                            else {
                                // exclude some filenames 
                                files = files.filter(function(file) {
                                    return ((file.indexOf("Thumbs.db") < 0) && (file.indexOf("_rejected_") < 0) && (file.indexOf(".DS_Store") < 0));
                                });
                                
                                if (files.length > 0) {

                                    logger.info('files found', files.length, correlated_path);

                                    queueHandler.bulkInsert(files, 'HF').then(function(result) {
                                        logger.info('dir files: bulk insert', correlated_path);
                                        callback();
                                    });
                                } else {
                                    //logger.info('dir files: no files', correlated_path);
                                    callback();
                                }
                            }
                        });

                    }, function(err) {
                        next(err);
                    });

                }
            ], function(err, result) {
                if (err) {
                    logger.error('staging error:', err);
                } else {
                    logger.info('scan staging completed');
                }

                //stagingJob.start(stagingIntervalTime, scanStaging);
            });
        };

        stagingJob.startNow(stagingIntervalTime, scanStaging);
        
        var taskSize = 0;

        var task = function(callback) {

            queueHandler.getPending().then(function(result) {

                var id = result.Id;
                var source = result.Source;
                var ingestTimeout = taskSize * 1000 * 30; //30 sec per image
                
                ingestTimeout = Math.max(ingestTimeout, minIngestTimeout);
                
                if (source === 'DF') {
                    //TODO: branch based on task source or branch before based on queue
                    dfWorker.execute(id, init_priorities, function(error, assetId, fileName, templatePath) {

                        if (error) {
                            logger.error('dfWorker execute', error);
                        }

                        dfIngestWorker.execute(assetId, fileName, templatePath, function() {

                            logger.info('ingest done ', assetId);

                            callback();
                        });

                    });
                } else {
                    //hot folder ingest worker
                    hfIngestWorker.execute(id, ingestTimeout, function(err) {
                        logger.info('ingest done ', id);
                        callback(err);
                    });
                }
            }).catch(function(error) {
                logger.error('GetPending', error);
            });
        };
        
        var isProcessing = false;
        
        var executeTasks = function() {

            //logger.info('execute tasks');

            queueHandler.getNumberPending().then(function(numPending) {

                if (numPending > 0 && !isProcessing) {

                    isProcessing = true;

                    //tasksJob.stop();

                    logger.info('starting tasks with this number pending', numPending);

                    var timestamp = moment();

                    var tasks = [];

                    if (numPending < batchSize) {
                        taskSize = numPending;
                    } else {
                        taskSize = batchSize;
                    }

                    for (var i = 0; i < taskSize; i++) {
                        tasks.push(task);
                    }

                    logger.info('task length', tasks.length);

                    async.parallel(tasks,
                        function(err, results) {
                            if (err) {
                                logger.error('Tasks', err);
                            }
                            else {
                                var newTimestamp = moment();
                                var elapsedTime = newTimestamp.diff(timestamp);

                                logger.info('Batch complete:', elapsedTime);


                            }

                            isProcessing = false;
                            //tasksJob.start(tasksIntervalTime, executeTasks);
                        }
                    );
                }
            });
        };

        var tasksJob = new job();
        tasksJob.startNow(tasksIntervalTime, executeTasks);
    };

    exports.start = function() {
        init().then(function() {
            execute();
        }).catch(function(err) {
            logger.error(err);
        });
    };
})();

